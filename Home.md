 **curl** is a command line tool for transferring data with URL syntax and **libcurl** is the library curl uses to do its job.

This wiki is meant as a way for users to easily contribute to the project. Additional documentation, hints, guides, examples and more. The main curl web site is at https://curl.se/ and it contains a vast amount of documentation and resources. There's no point in duplicating them here.

## An editable wiki!

This wiki is editable for users. It is not a security problem so stop reporting it. **All and any vandalizing done in the wiki will be reported as abuse.**