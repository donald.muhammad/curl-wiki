On March 20 2023 we plan to bump version and ship curl and libcurl version 8. This is curl's 25th birthday.

We will however not break compatibility or ABI because of that. Moving to version 8 will be just as smooth as any other release.