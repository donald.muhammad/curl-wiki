# WebSockets in libcurl - draft

For a long time people have expressed wishes and ideas about getting WebSockets support added to curl. Every year in the annual survey a large portion of users say they'd like it.

## Specs

- [RFC 6455 - WebSocket](https://datatracker.ietf.org/doc/html/rfc6455)
- [RFC 7692 - Compression](https://datatracker.ietf.org/doc/html/rfc7692)
- [RFC 8441 - Bootstrapping WebSockets with HTTP/2](https://datatracker.ietf.org/doc/html/rfc8441)
- [i-d Bootstrapping WebSockets with HTTP/3](https://www.ietf.org/archive/id/draft-ietf-httpbis-h3-websockets-00.html)

## Aim for what's used

WebSockets is highly extensible. This described API and first implemention do
not aim to support all existing and future extensions. But we should not write
the API or implementation to necessarily prevent us from implementing and
providing support for more extensions in a future.

We should support basic WebSockets and the "common" existing extensions to
make this WebSockets support usable for *most* use cases.

## The curl tool

Due to it being a transport for anything, it isn't ideal for the command line tool. It could possibly be made to work like TELNET does:

 - read from stdin and pass it on to the server
 - send data from server to stdout

Makes it work similar to 'nc'.

# Proposed API

- Provide a mode (`WS_ALONE`) that makes `CONNECT_ONLY`-style: only the
  WebSockets upgrade dance and then return to the application for
  `curl_easy_perform()`.
- New functions for recv/send so that we can pass on extra flags for
  websockets use (like end of packet flag, compression, binary/text etc)

## Outstanding questions

- do we need options for HTTP/2 ?

- how to set/change the maximum allowed message-size?

- what about other types like in a future extension? Should we reserve or do
  something for that possibility?

- do we need a `curl_ws_poll()` for the `WS_ALONE` use case? It could wait
  for websockets activity and transparently handle ping/pongs.

## `CURLOPT_URL`

Setting a URL with the scheme `ws://` or `wss://` marks this as a WebSockets
transfer.

## `curl_ws_send`

    curl_ws_send( easy, buffer, buflen, &sent, sendflags );

**sendflags** is a bitmask featuring the following flags:

- `CURLWS_TEXT` - this is text data
- `CURLWS_BINARY` - this is binary data
- `CURLWS_NOCOMPRESS` - no-op if there’s no compression anyway
- `CURLWS_FIN` - this is the end fragment of the message, if this is not set
                  it implies that there will be another fragment coming.
- `CURLWS_CLOSE` - close this transfer
- `CURLWS_PING` - send this as a ping
- `CURLWS_PONG` - send this as a pong

## `curl_ws_recv`

    curl_ws_recv( easy, buffer, buflen, &recvflags )

This function returns as much as possible of a received WebSockets data
*fragment*.

**recvflags** is a bitmask featuring the following (incoming) flags:

- `CURLWS_TEXT` - this is text data
- `CURLWS_BINARY` - this is binary data
- `CURLWS_FIN` - this is also the final fragment of a message
- `CURLWS_CLOSE` - this transfer is now closed
- `CURLWS_PING` - this is a ping

## `curl_ws_poll`?



## `CURLOPT_WS_OPTIONS`

A new *setopt() option that sets a bitmask:

- `CURLWS_ALONE` - ask for "stand alone" control using the easy API
- `CURLWS_COMPRESS` - negotiate compression for this transfer
- `CURLWS_PINGOFF` - disable automated ping/pong handling

## `CURLOPT_WS_WRITEFUNCTION`

This sets a websockets write callback to which libcurl will deliver incoming
*messages*.

    int message_callback(CURL *easy, char *data, size_t len,
                         unsigned int msgflags);

**msgflags** is a bitmask featuring the following (incoming) flags:

- `CURLWS_TEXT` - this is text data
- `CURLWS_BINARY` - this is binary data
- `CURLWS_CLOSE` - this transfer is now closed
- `CURLWS_PING` - this is a ping (if enabled)

## Multi interface

We want the API to allow for and handle *any* amount of concurrent WebSocket
transfers *in addition* to other libcurl transfers using the same multi
handle.

Therefore, we want WebSockets transfers able to deliver data using a
"WebSocket write callback" for the easy handle. From within that callback, we
should allow `curl_ws_send()` calls, and probably further `curl_ws_recv()`
calls as well.

## Mockup client psuedo source code using `WS_ALONE`

~~~c
CURLcode result;
CURL *ws = curl_easy_init();
curl_easy_setopt(ws, CURLOPT_URL, "ws://websockets.example.org");

/* only do the initial bootstrap */
curl_easy_setopt(ws, CURLOPT_WS_OPTIONS, CURLWS_ALONE);

result = curl_easy_perform(ws);

if(CURLE_OK == result) {
  /* this means it actually negotiated WebSockets successfully */

  /* send data */
  curl_ws_send();

  /* recv data */
  curl_ws_recv();

  /* wait for data to arrive */
  /* just using select() or curl_ws_poll() ? */
}

curl_easy_cleanup(ws); /* done */
~~~

## Mockup client psuedo source code using, easy interface with callbacks

~~~c

static int write_cb( ... char *data, size_t len ... ) 
{
  /* WebSocket data from the peer */

  /* If we want to send something here, use curl_ws_send() */
}

CURLcode result;
struct customstuff writep;
CURL *ws = curl_easy_init();
curl_easy_setopt(ws, CURLOPT_URL, "ws://websockets.example.org");

curl_easy_setopt(ws, CURLOPT_WS_WRITEFUNCTION, write_cb);
curl_easy_setopt(ws, CURLOPT_WS_WRITEDATA, &writep);

result = curl_easy_perform(ws);

curl_easy_cleanup(ws); /* done */
~~~

This method also works for the multi interface and then it can do many
parallel transfers and coexist with other protocol transfers in the same main
loop. Also even-based.

# Implementation

There's no implementation yet and the work has yet to be started.

It seems clever to base the implementation on an existing websockets library
for the bits over the wire at least.
[libwebsockets.org](https://libwebsockets.org/) looks like a viable contender.

It is expected that the API and details in this description will need updates
and polish once the implementation starts.

# Ping pong

By default libcurl does ping/pongs transparently without involving the
application. They can be set to "manual mode" with `CURLOPT_WS_OPTIONS`.

# Credits

People who have contributed thoughts and design ideas for this work:

Daniel Stenberg, Dmitry Karpov, Felipe Gasper, Stefan Eissing, Weston Schmidt
