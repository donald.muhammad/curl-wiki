#!/usr/bin/bash
cwdd=$(dirname $0)
cd $cwdd
git clone https://github.com/curl/curl.wiki.git
cp -fr $cwdd/curl.wiki/* $cwdd
sleep 1
rm -fr $cwdd/curl.wiki
sleep 1
git add *
git commit -S -m 'Mirror updates'
git push
